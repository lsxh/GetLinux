# 前言

> 多数情况下 我们都喜欢尝试各种不同的Linux 但是零散的Linux版本，以及未知的各种情况 让我们在前进的道路上留下不少的阻力与麻烦 我创建这个网站为促使Linux版本集中化 。

本网站收集了大部分的Linux发行版本与常用的国内镜像源 ，通过测试 发布其诸如软件包管理 桌面环境等相关特性。希望有志同道合的朋友可以向我们递投你的实践与心得。

在开始我们的尝试之前，相信你已经对Linux有了很深的了解。如果你对於这件事还有疑惑，下面一些网站可能对你有所帮助。当然，在此之前你也可以点击[Linux安装基础](./knowledge-base/base/list.md)，通过我们的文章来了解基本的Linux相关知识

1. [Linux教程|菜鸟教程](http://www.runoob.com/linux/linux-tutorial.html)

1. [Linux视频教程|慕课](https://www.imooc.com/course/list?c=linux)

1. [鸟哥的私房菜](http://linux.vbird.org/)

1. [Linux就该这样学](http://www.linuxprobe.com/)

---
一些与Linux有关并且随时可能对你有帮助的网站：

1. [kernel.org](https://www.kernel.org/ "Linux内核下载")

1. [Linux中国](https://linux.cn/ "技术文章分享，期待对你有所帮助")

1. [我是菜鸟](https://imcn.me/ "我是菜鸟Linux资讯")

<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?4223bde49487a942b437910913808dfa";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
# Summary

* [前言](README.md)
* [基础](./knowledge-base/base/list.md)
* [目录](list.md)
  * [redhat系列](list/redhat-series.md)
    * [RedHat Enterprise](list/redhat/rhel.md)
      * [RedHat安装](list/redhat/rhel/install.md)
      * [RedHat配置](list/redhat/rhel/config.md)
    * [Fedora](list/redhat/fedora.md)
      * [Fedora安装](list/redhat/fedora/install.md)
      * [Fedora配置](list/redhat/fedora/config.md)
    * [CentOS](list/redhat/centos.md)
      * [CentOS安装](list/redhat/centos/install.md)
      * [CentOS配置](list/redhat/centos/config.md)
      * [CentOS安装LNMP环境](list/redhat/centos/install-lnmp.md)
      * [CentOS美化](list/redhat/centos/beautify.md)
  * [debian系列](list/debian-series.md)
    * [Debian](list/debian/debian.md)
      * [Debian安装](list/debian/debian/install.md)
      * [Debian美化](list/debian/debian/beautify.md)
      * [Debian配置](list/debian/debian/config.md)
    * [Ubuntu](list/debian/ubuntu.md)
      * [Ubuntu安装](list/debian/ubuntu/install.md)
      * [Ubuntu美化](list/debian/ubuntu/beautify.md)
      * [Ubuntu配置](list/debian/ubuntu/config.md)
        * [语言设置](list/debian/ubuntu/language.md)
        * [root用户设置](list/debian/ubuntu/rootconfig.md)
    * [deepin](list/debian/deepin.md)
      * [deepin安装](list/debian/deepin/install.md)
      * [deepin美化](list/debian/deepin/beautify.md)
      * [deepin配置](list/debian/deepin/config.md)
        * [root用户设置](list/debian/deepin/rootconfig.md)
        * [命令安装软件](list/debian/deepin/aptInstall.md)
        * [lnmp环境搭建](list/debian/deepin/lnmpconfig.md)
    * [Linux Mint](list/debian/linuxMint.md)
      * [Mint安装](list/debian/mint/install.md)
      * [Mint配置](list/debian/mint/config.md)
    * [Elementary OS](list/debian/elementaryOS.md)
      * [Elementary安装](list/debian/elementary/install.md)
      * [Elementary美化](list/debian/elementary/beautify.md)
    * [优麒麟](list/debian/kylin.md)
      * [kylin安装](list/debian/kylin/install.md)
      * [kylin配置](list/debian/kylin/config.md)
        * [语言设置](list/debian/kylin/language.md)
  * [其他系列](list/other-series.md)
    * [Arch](list/others/archlinux/archlinux.md)
      * [Arch安装](list/others/archlinux/archlinux/install.md)
      * [Arch配置](list/others/archlinux/archlinux/config.md)
    * [OpenSUSE](list/others/opensuse/opensuse.md)
  * [UNIX系列](list/unix-series.md)
* [知识库](knowledge-base/list.md)
  * [基础](knowledge-base/base/list.md)
    * [制作U盘启动盘](knowledge-base/base/Make-U-Disk/list.md)
      * [Rufus制作](knowledge-base/base/Make-U-Disk/rufus.md)
      * [PowerISO制作](knowledge-base/base/Make-U-Disk/powerISO.md)
      * [UltraISO制作](knowledge-base/base/Make-U-Disk/ultraISO.md)
    * [磁盘相关知识](knowledge-base/base/Disk-knowledge/list.md)
      * [簇大小](knowledge-base/base/Disk-knowledge/ClusterSize.md)
      * [文件系统](knowledge-base/base/Disk-knowledge/FileSystem.md)
      * [镜像类型](knowledge-base/base/Disk-knowledge/DOS-ISO-DD.md)
        * [DD镜像](knowledge-base/base/Disk-knowledge/DD.md)
        * [ISO镜像](knowledge-base/base/Disk-knowledge/ISO.md)
        * [FreeDOS镜像](knowledge-base/base/Disk-knowledge/FreeDOS.md)
      * [磁盘分区](knowledge-base/base/Disk-knowledge/Disk-Partition-list.md)
        * [什么是分区](knowledge-base/base/Disk-knowledge/what-is-disk-partition.md)
        * [分区方案](knowledge-base/base/Disk-knowledge/partition-scheme.md)
  * [软件包管理](knowledge-base/Package-Management/list.md)
    * [dpkg包管理](https://wiki.deepin.org/index.php?title=软件包管理#.E5.89.8D.E8.A8.80)
  * [常用软件安装](software/index.md)
    * [安装JDK](software/install-jdk.md)
  * [Linux历史](knowledge-base/history/history.md)
  * [Linux牛人](knowledge-base/supermen/supermen.md)
  * [发行版本集合](listall.md)
* [镜像](images.md)
* [投稿](contribute.md)
* [关于我们](about.md)
